/* ************************************************************************************
* File:    eeprom.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## eeprom.h - read/write interface to ATTiny13 eeprom memory

The module provides basic read/write of the microcontroller eeprom.

**Tip:** Writing a specific bytecode to a specific location of the EEPROM is an easy way to know if
it has been used before or if initial data should be written. If the bytecode is missing, then it can be
considered "unformatted" and any initial data should be written.
Alternately, you could test the values of every read operation.

--------------------------------------------------------------------------
--- */


#ifndef _EEPROM_H_
#define _EEPROM_H_

#include <avr/eeprom.h>

/***************************************************************************************************
  void (uint16_t v_eepromAddres_u16, uint8_t v_eepromData_u8)
 ***************************************************************************************************
 * I/P Arguments: uint16_t: eeprom_address at which eeprom_data is to be written
                  uint8_t: byte of data to be to be written in eeprom.
 * Return value	: none
 * description: This function is used to write the data at specified EEPROM_address..
 **************************************************************************************************/
void eepromWriteByte(uint16_t addr, uint8_t data)
{
	// Wait for completion of previous write.
	// EEWE will be cleared by hardware once Eeprom write is completed.
	while(EECR & (1<<EEPE))
		;

	EEAR = addr;			//Load the eeprom address and data
	EEDR = data;

	EECR |= (1<<EEMPE);		// Eeprom Master Write Enable
	EECR |= (1<<EEPE);		// Start eeprom write by setting EEWE
}


/***************************************************************************************************
               uint8_t (uint16_t v_eepromAddres_u16)
 ***************************************************************************************************
 * I/P Arguments: uint16_t: eeprom_address from where eeprom_data is to be read.
 * Return value	: uint8_t: data read from Eeprom.
 * description: This function is used to read the data from specified EEPROM_address.
 ***************************************************************************************************/
uint8_t eepromReadByte(uint16_t addr)
{
	// Wait for completion of previous write.
	// EEWE will be cleared by hardware once Eeprom write is completed.
	while(EECR & (1<<EEPE))
		;

	EEAR = addr;  			//Load the eeprom address and data
	EECR |= (1<<EERE);		// start eeprom read by setting EERE

	return EEDR;			// Return data from data register
}

#endif
