/* ************************************************************************************
* File:    main.c
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---
# BADGELIFE_TINY13

** Simple functions for developing ATTiny13 LED and Buzzer projects**

This code was originally developed for the creation of holiday ornaments.
However, the code is useful in a range of projects which use the ATTiny13 8bit microchip.
The code is very compact and does not require any Arduino or other external libraries.

There are 5 pins 'easily' usable on an ATTiny13. This code sues 4 pins for charlieplexed LEDs
and 1 pin for a piezo buzzer. This supports up to 12 LEDs.
It would be easy to eliminate the buzzer and use all 5 pins for charlieplexed LEDs.
This configuration would support up to 20 LEDs.

**NOTE:** The ATTiny13 internal clock runs at 9.6Mhz. This code uses DIV8 to run at 1.2Mhz

The `files` folder contains an example KiCAD project for a circular PCB.
![Badgelife ATTiny13 KiCAD Example](https://gitlab.com/bradanlane/badgelife_tiny13/-/raw/master/files/badgelife_tiny13.png)

--------------------------------------------------------------------------
--------------------------------------------------------------------------

--- */



#ifdef F_CPU
#undef F_CPU
#endif
#define F_CPU 1200000L // default to (9.6 / 8) = 1.2MHz clock

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
#define TIME_SIGNATURE 4 // 4/4 time
#define TRANSPOSE_OCTAVES 1

// ---------------------------------------------------------------------------------------------------------------------

uint8_t g_note_offset;	// a global to make it easy to shift which LED is the starting point for matching up to musical notes


#include "pins.h"
#include "leds.h"
#include "sleep.h"
#include "eeprom.h"
#include "sound.h"

#include "song_test.h"


// ---------------------------------------------------------------------------------------------------------------------
//  remember the starting LED so we can shift the starting position each run
// ---------------------------------------------------------------------------------------------------------------------
void update_note_offset() {
	uint8_t mode;
	mode = eepromReadByte(NOTE_OFFSET_ADDR);
	mode = (mode + 1) % NOTE_0;
	eepromWriteByte(NOTE_OFFSET_ADDR, mode);
	g_note_offset = mode;
}


// ---------------------------------------------------------------------------------------------------------------------
// simple function to read the music (packed bytes) and play it
// ---------------------------------------------------------------------------------------------------------------------
void play_song(const uint8_t *song, int len) {
	int i;
	uint8_t n;

	for (i = 0; i < len; i++) {
		n = pgm_read_byte(&(song[i]));
		soundPlay(n /*song[i] */);
	}

	soundPlay(0); // silence
}

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

int main() {
	// the follow code is helpful for testing but not typically used for normal exectution
#if 1
	{
		cpAllOn(2);	// *1 seconds
		for (uint8_t i = 0; i < CP_MAX; i++) {
			cpLedOn(i);
			_delay_ms(1000);
		}
		cpAllOff();
	}
#endif

	while (1) {
		play_song(melody, sizeof(melody));

		//_delay_ms(1000);
		update_note_offset();

		cpAllOn(2); // *1 seconds

		go_to_sleep();
	}

	return (0);
}
