#ifndef _PINS_H_
#define _PINS_H_

// Pins Used
#define PIN_A PB1
#define PIN_B PB2
#define PIN_C PB3
#define PIN_D PB4

// NOTE: PB2 is now our mode detect button, wake button, and used by the charliepixels
#define PIN_BUTTON PB2

#define BUZZER_PIN PB0


// we could start at 0x0 but we leave the first 16 bytes alone just in case we want to have some system level features like a version, signature, or ID number
#define MUSIC_MODE_ADDR		0x10
#define NOTE_OFFSET_ADDR	0x11

#endif // _PINS_H_
