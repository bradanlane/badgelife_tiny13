/* ************************************************************************************
* File:    leds.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## leds.h - control a set of charlieplexed LEDs using brute force *one on at a time*.
A timer could be used but is not needed for simple projects.

While only a single LED is every ON at any instant, rapidly switching between LEDs allows the appearance fo multiple LEDs on at a time *however at a lower brightness*.

**Tip:** Charlieplexing require 2 pins for 2 LEDs; 3 pins for 6 LEDs; 4 pins for 12 LEDs; 5 pins for 20 LEDs.

--------------------------------------------------------------------------
--- */
#ifndef _CHARLIEPLEX_H_
#define _CHARLIEPLEX_H_

#ifdef LEDS_SIMPLE
// there must be a definition for `LEDS_AVAILABLE` and the data for `led_pins[]` defined before loading leds.h

void cpAllOff() {
	for (uint8_t i = 0; i < LEDS_AVAILABLE; i++) {
		DDRB  &= ~(led_pins[i]);
		PORTB &= ~(led_pins[i]);
	}
}
void cpAllOn(uint8_t seconds) {
	for (uint8_t i = 0; i < LEDS_AVAILABLE; i++) {
		DDRB  |= (led_pins[i]);
		PORTB |= (led_pins[i]);
	}
	for (uint8_t i = 0; i < seconds; i++)
		_delay_ms(1000);
	cpAllOff();
}

void cpLedOn(uint8_t led_num) { //led_num must be from 0 to 9
	led_num = (led_num + LEDS_AVAILABLE) % LEDS_AVAILABLE;	// we add MAX to avoid wrapping backwards at ZERO
	// clear any existing LED
	cpAllOff();
	// set the new LED
	DDRB  |= (led_pins[led_num]);
	PORTB |= (led_pins[led_num]);
}

#else	// not LEDS_SIMPLE

#define ALL_LEDS ((1<<PB1) | (1<<PB2) | (1<<PB3) | (1<<PB4))

#define CP_MAX 10	// number of LEDs; the max with 4 pins is 12 LEDs but we have never used more than 10

// allow up-stream code to set a lower limit for the number of available LEDs
#ifndef LEDS_AVAILABLE
#define LEDS_AVAILABLE CP_MAX
#endif

//DDRB direction config for each LED (1 = output)
const uint8_t led_dir[CP_MAX] = {
	( 1<<PB1 | 1<<PB2 ), //1
	( 1<<PB1 | 1<<PB2 ), //2
	( 1<<PB1 | 1<<PB3 ), //3
	( 1<<PB1 | 1<<PB3 ), //4
	( 1<<PB1 | 1<<PB4 ), //5
	( 1<<PB1 | 1<<PB4 ), //6
	( 1<<PB2 | 1<<PB3 ), //7
	( 1<<PB2 | 1<<PB3 ), //8
	( 1<<PB2 | 1<<PB4 ), //9
	( 1<<PB2 | 1<<PB4 ), //10
};

//PORTB output config for each LED (1 = High, 0 = Low)
const uint8_t led_out[CP_MAX] = {
	( 1<<PB2 ),//1
	( 1<<PB1 ),//2
	( 1<<PB3 ),//3
	( 1<<PB1 ),//4
	( 1<<PB4 ),//5
	( 1<<PB1 ),//6
	( 1<<PB3 ),//7
	( 1<<PB2 ),//8
	( 1<<PB4 ),//9
	( 1<<PB2 ),//10
};

void cpAllOff() {
	DDRB &= ~(ALL_LEDS);	// set all as input
	PORTB &= ~(ALL_LEDS);	// set all low
}

// set both pins of LED to output; set port pin HIGH to turn on an LED
// only one LED is on at any time
void cpLedOn(uint8_t led_num) { //led_num must be from 0 to 9
	led_num = (led_num + LEDS_AVAILABLE) % LEDS_AVAILABLE;	// we add MAX to avoid wrapping backwards at ZERO
	// clear any existing LED
	cpAllOff();
	// set the new LED
	DDRB |= led_dir[led_num];
	PORTB = led_out[led_num];
}


void cpAllOn(uint8_t seconds) {
	uint8_t i;
	uint16_t d;

	d = seconds * (1000/LEDS_AVAILABLE);	// number of iteratations at 50Hz

	for (; d > 0; d--) {
		for (i = 0; i < LEDS_AVAILABLE; i++) {
			cpLedOn(i);
			_delay_ms(1);	// with 10 LEDs this is 20ms or 50Hz refresh rate for 10 LEDS (its a bit faster with fewer LEDS)
		}
	}
	cpAllOff();
}
#endif // not LEDS_SIMPLE

#endif // _CHARLIEPLEX_H_
