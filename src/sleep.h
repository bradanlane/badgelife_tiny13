/* ************************************************************************************
* File:	sleep.h
* Date:	2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## sleep.h - sleep / wake functions

It preserves all registers and timers during the sleep cycle.
The wake is detected from INT0.

--------------------------------------------------------------------------
--- */


#ifndef _SLEEP_H_
#define _SLEEP_H_

void go_to_sleep() {
	// the following will save some power during power down sleep
	ADCSRA &= ~(1 << ADEN); // disable ADC (before power-off)
	ACSR |= (1 << ACD);		// disable the analog comparator
	// minimize power usage by setting all pins as input low
	DDRB = 0; //|= ((1 << PB0) | (1 << PB1) | (1 << PB2) | (1 << PB3) | (1 << PB4));
	PORTB = 0;
	//PORTB |= ((1 << PB0) | (1 << PB1) | (1 << PB2));

	DDRB &= ~(1 << PIN_BUTTON);					// Input: where a button is attached
	PORTB |= (1 << PIN_BUTTON);					// Pull up using software, just to be sure

	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	cli();

	#ifdef SLEEP_ALTERNATE
	PCMSK |= ((1 << PCINT2));
	GIFR |= (1 << PCIF);	// clear any previous pin interupts
	GIMSK |= (1 << PCIE);
	#else
	// need to set pin to input pull up DDRB
	GIMSK |= (1 << INT0);
	MCUCR &= ~((1 << ISC01) | (1 << ISC00));	// INT0 on low level
	#endif

	// NOTE: PB2 is both our wake button and used by the charliepixels
	sei();					 					// ensure interrupts enabled so we can wake up again
	sleep_enable();
	sleep_cpu();							 	// go to sleep

	// execution resumes here after wake from sleep

	cli();
	#ifdef SLEEP_ALTERNATE
	GIFR |= (1 << PCIF);	// clear any previous pin interupts
	GIMSK &= ~(1 << PCIE);
	#else
	GIMSK &= ~(1 << INT0);
	#endif
	sei();
	sleep_disable();
	//PORTB &= ~((1 << PB3) | (1 << PB4));

}

#ifdef SLEEP_ALTERNATE
ISR(PCINT0_vect) {
}
#else
ISR(INT0_vect) {
}
#endif

#endif
