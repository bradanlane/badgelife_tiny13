#ifndef _SONG_H_
#define _SONG_H_

// notes:                C   C#  D  D#   E   F   F#  G   G#  A   A#  B
// N(n,o,d)
const uint8_t melody[] PROGMEM = {
	// do you want to build a
	N(NOTE_C, 1, QUARTER),
	N(NOTE_CS, 1, QUARTER),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_DS, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_F, 1, QUARTER),
	N(NOTE_FS, 1, QUARTER),
	N(NOTE_G, 1, QUARTER),
	N(NOTE_GS, 1, QUARTER),
	N(NOTE_A, 2, QUARTER),	// this is a harmonic issue with the buzzer
	N(NOTE_AS, 2, QUARTER),
	N(NOTE_B, 2, QUARTER),
};

#endif
