/* ************************************************************************************
* File:    sound.h
* Date:    2020.05.04
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

## sound.h - single musical note sound generator

This code is written for the ATTiny13. However, within the comments section you will find
some references to the ATTiny85 *which because of it's clock speed prescaler options makes a bit nicer music*.

The frequency table is the musical scale starting at C3.

 The functions use a **packed byte** storage for notes. A single byte holds teh note, duration, and octave.
 Optional transposition is handled by a compiler flag.

A single byte has 2 bits for the duration, 2 bits for the octave, and 3 bits for the note.

`ddoonnnn = note duration, octave, note (C thru B)`

--------------------------------------------------------------------------
--- */

#ifndef _SOUND_H_
#define _SOUND_H_


// we can pack the music into 8 bits: ddoonnnn = note duration, octave, note (C thru B)
/*
	to extend the capability for music, we use two special 'notes'
	NOTE_DOTTED means it is 1.5x the duration
	NOTE_TRIPLET means the 3 notes fit in the given duration
	these special 'notes' are prefixes and get the actual note and octave from the data which follows
	NOTE_DOTTED uses the next byte
	NOTE_TRIPLET uses the next 3 bytes
*/

#define NOTE_DOTTED		14
#define NOTE_TRIPLET	15


// macro to pack notes
#define N(n, o, d)						( (((d)&0x03) << 6) | (((o)&0x03) << 4) | ((n)&0x0F) )
#define D(n, o, d)						N(NOTE_DOTTED, o, d), N(n, 0, 0)
#define T(n1, o1, n2, o2, n3, o3, d)	N(NOTE_TRIPLET, o, d), N(n1, o1, 0), N(n2, o2, 0), N(n3, o3, 0)


// macros to unpack notes
#define GET_NOTE(n) (((n)&0x0F))
#define GET_OCTAVE(n) (((n)&0x30) >> 4)
#define GET_DURATION_RAW(n) (((n)&0xC0) >> 6)

// basic music note durations (no triplets dotted notes, etc.
#define WHOLE 0x03   // 0b11
#define HALF 0x02	// 0b10
#define QUARTER 0x01 // 0b01
#define EIGHTH 0x00  // 0b00

#ifndef TEMPO
#define TEMPO 200 // milliseconds for an eighth note
#endif

// attiny13a clock is 9.6mhz and 1.2mhz so the follow tones will be off by 20%

// notes:                      C    C#   D    D#   E    F    F#   G    G#   A    A#   B
const uint8_t notes_scale[] = {0, 239, 226, 213, 201, 190, 179, 169, 160, 151, 142, 134, 127};
#define NOTE_0 0
#define NOTE_REST NOTE_0
#define NOTE_C 1
#define NOTE_CS 2
#define NOTE_DF NOTE_CS
#define NOTE_D 3
#define NOTE_DS 4
#define NOTE_EF NOTE_DS
#define NOTE_E 5
#define NOTE_F 6
#define NOTE_ES NOTE_F
#define NOTE_FS 7
#define NOTE_G 8
#define NOTE_GS 9
#define NOTE_A 10
#define NOTE_AS 11
#define NOTE_BF NOTE_AS
#define NOTE_B 12


void soundOff() {
	DDRB &= (~(1 << BUZZER_PIN));
	PORTB |= (1 << BUZZER_PIN); // this should use internal pullups
}

void soundOn(uint8_t note, uint8_t octave) {
	soundOff();
	if (!note)
		return;

	if (note != NOTE_REST) {
		int prescaler = 0;

		if (note > 12) {
			note = 12; // this is a safety but will generate bad notes if 'note' is not between 1..12
		}

		uint8_t pitch = notes_scale[note];

		// prescaler on attiny85 is 1, 2,  4,  8 (,16,32)
		// on attiny85 we would the octaves directly from the scaler

		// prescaler on attiny13 is 1, 8, 64    (,256,1024)
		// on attiny13 we divide the pitch to get the octaves between the scalers

		// because of the above transpose option we may have octaves in the range of 0..4

		// reference: https://github.com/lpodkalicki/blog/blob/master/avr/attiny13/007_tone_generator/main.c
		// from the sample code, we are mapping 0..4 to octave 3..7

		prescaler = (1 << CS01);
		if (octave < 2)
			prescaler |= (1 << CS00);

		if ((octave == 0) || (octave == 3))
			pitch /= 2;
		if ((octave == 1) || (octave == 4))
			pitch /= 4;

		DDRB |= (1 << BUZZER_PIN);
		OCR0A = pitch;

		TCCR0A |= ((1 << WGM01)); // set timer mode to CTC
		TCCR0A |= (1 << COM0A0);  // connect PWM pin to Channel A of Timer0

		TCCR0B = prescaler;
		//TCCR1 = (1 << CTC1) | (prescaler << CS10); // this is specific to the ATTiny85 registers for prescaler (CS13:CS12:CS11:CS10)
	}
}


void songPlay(const uint8_t *song, int len) {
	int i;
	uint8_t n;

	for (i = 0; i < len; i++) {
		n = pgm_read_byte(&(song[i]));

		// unpack the note
		uint8_t note = GET_NOTE(n);
		uint8_t octave = GET_OCTAVE(n) + TRANSPOSE_OCTAVES;
		uint8_t duration = GET_DURATION_RAW(n);

		// need to map the raw duration bits to a value
		// if the music needs sixteenth notes, then its up to the caller to double the temp and sacrifice the whole notes
		switch (duration) {
			case WHOLE: {		duration = 2 * TIME_SIGNATURE;		} break;
			case HALF: {		duration = 4;						} break;
			case QUARTER: {		duration = 2;						} break;
			case EIGHTH:
			default: { 			duration = 1; 						} break;
		}

		uint16_t duration_ms = duration * TEMPO;
		uint8_t note_count = 1;

		// if this is a dotted note, then we extend the duration by 50%
		if (note == NOTE_DOTTED) {
			duration_ms += (duration_ms / 2);
			// get the actual note from the next byte in the song
			i++;
			n = pgm_read_byte(&(song[i]));
			note = GET_NOTE(n);
		}

		// if this is a triplet, then each of the 3 notes gets 1/3rd
		if (note == NOTE_TRIPLET) {
			note_count = 3;
			duration_ms = (duration_ms / 3);
			// get the actual note from the next byte in the song
			i++;
			n = song[i];
			note = GET_NOTE(n);
			octave = GET_OCTAVE(n);
		}

		for (uint8_t k = 0; k < note_count; k++) {
			// led on for notes but not for rests
			if ((note > 0))
				cpLedOn(note + g_note_offset -1);
			// play note
			if (note)
				soundOn(note, octave);

#ifdef STACCATO
			duration_ms -= 10;
#endif
			// this works well if the LEDs have been disabled; or with a hardware interrupt
			for (uint16_t j = 0; j < (duration_ms); j++) {
				_delay_ms(1);	// TODO there must be a better way
			}

			// tiny compensation for notes of different length
			// we used to compute the compensation as a gap between notes but this has the same effect without a variable gap
			uint16_t gap_compensation = (duration_ms * 5) / TEMPO;
			while (gap_compensation) {
				_delay_ms(1);
				gap_compensation--;
			}

			// TODO determine if we have a 'tied note' and do not stop sound between

			if (note_count > 1) {
				// get next note
				i++;
				n = pgm_read_byte(&(song[i]));
				note = GET_NOTE(n);
				octave = GET_OCTAVE(n) + TRANSPOSE_OCTAVES;
			}
		}
		soundOff();
		cpAllOff();
		// tiny gap between notes
#ifdef STACCATO
		_delay_ms(10);
#endif
		_delay_ms(1);
	}
	cpAllOff();
}

// ---------------------------------------------------------------------------------------------------------------------


#endif /* _SOUND_H_ */
