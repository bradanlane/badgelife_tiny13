# BADGELIFE_TINY13

**Simple functions for developing ATTiny13 LED and Buzzer projects**

This code was originally developed for the creation of holiday ornaments.
However, the code is useful in a range of projects which use the ATTiny13 8bit microchip.
The code is very compact and does not require any Arduino or other external libraries.

There are 5 pins 'easily' usable on an ATTiny13. This code sues 4 pins for charlieplexed LEDs
and 1 pin for a piezo buzzer. This supports up to 12 LEDs.
It would be easy to eliminate the buzzer and use all 5 pins for charlieplexed LEDs.
This configuration would support up to 20 LEDs.

**NOTE:** The ATTiny13 internal clock runs at 9.6Mhz. This code uses DIV8 to run at 1.2Mhz

The `files` folder contains an example KiCAD project for a circular PCB.
![Badgelife ATTiny13 KiCAD Example](https://gitlab.com/bradanlane/badgelife_tiny13/-/raw/master/files/badgelife_tiny13.png)

--------------------------------------------------------------------------
--------------------------------------------------------------------------



## sound.h - single musical note sound generator

This code is written for the ATTiny13. However, within the comments section you will find
some references to the ATTiny85 *which because of it's clock speed prescaler options makes a bit nicer music*.

The frequency table is the musical scale starting at C3.

 The functions use a **packed byte** storage for notes. A single byte holds teh note, duration, and octave.
 Optional transposition is handled by a compiler flag.

A single byte has 2 bits for the duration, 2 bits for the octave, and 3 bits for the note.

`ddoonnnn = note duration, octave, note (C thru B)`

--------------------------------------------------------------------------


## eeprom.h - read/write interface to ATTiny13 eeprom memory

The module provides basic read/write of the microcontroller eeprom.

**Tip:** Writing a specific bytecode to a specific location of the EEPROM is an easy way to know if
it has been used before or if initial data should be written. If the bytecode is missing, then it can be
considered "unformatted" and any initial data should be written.
Alternately, you could test the values of every read operation.

--------------------------------------------------------------------------


## sleep.h - sleep / wake functions

It preserves all registers and timers during the sleep cycle.
The wake is detected from INT0.

--------------------------------------------------------------------------

