EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D4
U 1 1 5DACCC75
P 8200 3600
F 0 "D4" V 8147 3678 50  0000 L CNN
F 1 "LED" V 8238 3678 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8200 3600 50  0001 C CNN
F 3 "~" H 8200 3600 50  0001 C CNN
	1    8200 3600
	0    1    1    0   
$EndComp
$Comp
L Device:Battery_Cell BT1
U 1 1 5DACE21B
P 2150 2600
F 0 "BT1" V 2268 2696 50  0000 L CNN
F 1 "Battery_Cell" V 2250 2200 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_3034_1x20mm" V 2150 2660 50  0001 C CNN
F 3 "~" V 2150 2660 50  0001 C CNN
	1    2150 2600
	0    1    1    0   
$EndComp
$Comp
L Device:Buzzer BZ1
U 1 1 5DACF087
P 5300 2400
F 0 "BZ1" H 5452 2429 50  0000 L CNN
F 1 "Buzzer" H 5452 2338 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_CUI_CPT-9019S-SMT" V 5275 2500 50  0001 C CNN
F 3 "~" V 5275 2500 50  0001 C CNN
	1    5300 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5DAD0639
P 7300 2650
F 0 "D1" V 7247 2728 50  0000 L CNN
F 1 "LED" V 7338 2728 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7300 2650 50  0001 C CNN
F 3 "~" H 7300 2650 50  0001 C CNN
	1    7300 2650
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 5DAD0894
P 7700 2650
F 0 "D2" V 7739 2533 50  0000 R CNN
F 1 "LED" V 7648 2533 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7700 2650 50  0001 C CNN
F 3 "~" H 7700 2650 50  0001 C CNN
	1    7700 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D6
U 1 1 5DAD0B16
P 8600 3600
F 0 "D6" V 8639 3483 50  0000 R CNN
F 1 "LED" V 8548 3483 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8600 3600 50  0001 C CNN
F 3 "~" H 8600 3600 50  0001 C CNN
	1    8600 3600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5DAD58EE
P 1600 2600
F 0 "#PWR0101" H 1600 2350 50  0001 C CNN
F 1 "GND" H 1605 2427 50  0000 C CNN
F 2 "" H 1600 2600 50  0001 C CNN
F 3 "" H 1600 2600 50  0001 C CNN
	1    1600 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 2600 2050 2600
Wire Wire Line
	3800 2300 3800 2800
$Comp
L power:GND #PWR0102
U 1 1 5DAD6601
P 3800 4700
F 0 "#PWR0102" H 3800 4450 50  0001 C CNN
F 1 "GND" H 3805 4527 50  0000 C CNN
F 2 "" H 3800 4700 50  0001 C CNN
F 3 "" H 3800 4700 50  0001 C CNN
	1    3800 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4700 3800 4000
$Comp
L power:GND #PWR0103
U 1 1 5DAD776A
P 5150 2650
F 0 "#PWR0103" H 5150 2400 50  0001 C CNN
F 1 "GND" H 5155 2477 50  0000 C CNN
F 2 "" H 5150 2650 50  0001 C CNN
F 3 "" H 5150 2650 50  0001 C CNN
	1    5150 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5DADBC44
P 8200 2650
F 0 "D3" V 8147 2728 50  0000 L CNN
F 1 "LED" V 8238 2728 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8200 2650 50  0001 C CNN
F 3 "~" H 8200 2650 50  0001 C CNN
	1    8200 2650
	0    1    1    0   
$EndComp
$Comp
L Device:LED D5
U 1 1 5DADBC4A
P 8600 2650
F 0 "D5" V 8639 2533 50  0000 R CNN
F 1 "LED" V 8548 2533 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8600 2650 50  0001 C CNN
F 3 "~" H 8600 2650 50  0001 C CNN
	1    8600 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D8
U 1 1 5DADBC50
P 9100 3600
F 0 "D8" V 9047 3678 50  0000 L CNN
F 1 "LED" V 9138 3678 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9100 3600 50  0001 C CNN
F 3 "~" H 9100 3600 50  0001 C CNN
	1    9100 3600
	0    1    1    0   
$EndComp
$Comp
L Device:LED D10
U 1 1 5DADBC56
P 9500 3600
F 0 "D10" V 9539 3483 50  0000 R CNN
F 1 "LED" V 9448 3483 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9500 3600 50  0001 C CNN
F 3 "~" H 9500 3600 50  0001 C CNN
	1    9500 3600
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D7
U 1 1 5DADC655
P 9100 2650
F 0 "D7" V 9047 2728 50  0000 L CNN
F 1 "LED" V 9138 2728 50  0000 L CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9100 2650 50  0001 C CNN
F 3 "~" H 9100 2650 50  0001 C CNN
	1    9100 2650
	0    1    1    0   
$EndComp
$Comp
L Device:LED D9
U 1 1 5DADC65B
P 9500 2650
F 0 "D9" V 9539 2533 50  0000 R CNN
F 1 "LED" V 9448 2533 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 9500 2650 50  0001 C CNN
F 3 "~" H 9500 2650 50  0001 C CNN
	1    9500 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 3200 6550 3200
Wire Wire Line
	6550 3200 6550 2350
Wire Wire Line
	6550 2350 7300 2350
Wire Wire Line
	7300 2350 7300 2500
Wire Wire Line
	7300 2350 7700 2350
Wire Wire Line
	7700 2350 7700 2500
Connection ~ 7300 2350
Wire Wire Line
	6650 3300 6650 2900
Wire Wire Line
	6650 2900 7300 2900
Wire Wire Line
	7300 2900 7300 2800
Wire Wire Line
	7300 2900 7700 2900
Wire Wire Line
	7700 2900 7700 2800
Connection ~ 7300 2900
Wire Wire Line
	6050 3300 6650 3300
Wire Wire Line
	7700 2350 8200 2350
Wire Wire Line
	8200 2350 8200 2500
Connection ~ 7700 2350
Wire Wire Line
	8200 2350 8600 2350
Wire Wire Line
	8600 2350 8600 2500
Connection ~ 8200 2350
Wire Wire Line
	8600 2350 9100 2350
Wire Wire Line
	9100 2350 9100 2500
Connection ~ 8600 2350
Wire Wire Line
	9100 2350 9500 2350
Wire Wire Line
	9500 2350 9500 2500
Connection ~ 9100 2350
Wire Wire Line
	6050 3400 6750 3400
Wire Wire Line
	6750 3400 6750 3000
Wire Wire Line
	6750 3000 8200 3000
Wire Wire Line
	8200 3000 8200 2800
Wire Wire Line
	8200 3000 8600 3000
Wire Wire Line
	8600 3000 8600 2800
Connection ~ 8200 3000
Wire Wire Line
	6050 3500 6850 3500
Wire Wire Line
	6850 3500 6850 3100
Wire Wire Line
	6850 3100 9100 3100
Wire Wire Line
	9100 3100 9100 2800
Wire Wire Line
	9100 3100 9500 3100
Wire Wire Line
	9500 3100 9500 2800
Connection ~ 9100 3100
Wire Wire Line
	6650 3300 8200 3300
Wire Wire Line
	8200 3300 8200 3450
Connection ~ 6650 3300
Wire Wire Line
	8200 3300 8600 3300
Wire Wire Line
	8600 3300 8600 3450
Connection ~ 8200 3300
Wire Wire Line
	8600 3300 9100 3300
Wire Wire Line
	9100 3300 9100 3450
Connection ~ 8600 3300
Wire Wire Line
	9100 3300 9500 3300
Wire Wire Line
	9500 3300 9500 3450
Connection ~ 9100 3300
Wire Wire Line
	7350 3400 7350 3900
Wire Wire Line
	7350 3900 8200 3900
Wire Wire Line
	8200 3900 8200 3750
Wire Wire Line
	6750 3400 7350 3400
Connection ~ 6750 3400
Wire Wire Line
	8200 3900 8600 3900
Wire Wire Line
	8600 3900 8600 3750
Connection ~ 8200 3900
Wire Wire Line
	6850 3500 7250 3500
Wire Wire Line
	7250 3500 7250 4000
Wire Wire Line
	7250 4000 9100 4000
Wire Wire Line
	9100 4000 9100 3750
Connection ~ 6850 3500
Wire Wire Line
	9100 4000 9500 4000
Wire Wire Line
	9500 4000 9500 3750
Connection ~ 9100 4000
Text GLabel 3800 2300 1    50   Input ~ 0
VCC
Text GLabel 1650 3150 0    50   Input ~ 0
MISO
Text GLabel 1650 3450 0    50   Input ~ 0
MOSI
Text GLabel 1650 3350 0    50   Input ~ 0
SCK
Text GLabel 1650 3550 0    50   Input ~ 0
RESET
Text GLabel 1650 3250 0    50   Input ~ 0
VCC
$Comp
L power:GND #PWR0104
U 1 1 5DAE6B23
P 1700 3900
F 0 "#PWR0104" H 1700 3650 50  0001 C CNN
F 1 "GND" H 1705 3727 50  0000 C CNN
F 2 "" H 1700 3900 50  0001 C CNN
F 3 "" H 1700 3900 50  0001 C CNN
	1    1700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3150 1850 3150
Wire Wire Line
	1850 3250 1650 3250
Wire Wire Line
	1650 3350 1850 3350
Wire Wire Line
	1850 3450 1650 3450
Wire Wire Line
	1650 3550 1850 3550
Wire Wire Line
	1850 3650 1700 3650
Wire Wire Line
	1700 3650 1700 3900
Text GLabel 4750 1900 2    50   Input ~ 0
MISO
Text GLabel 4750 2400 2    50   Input ~ 0
MOSI
Text GLabel 4750 2200 2    50   Input ~ 0
SCK
Text GLabel 4750 2550 2    50   Input ~ 0
RESET
Wire Wire Line
	4750 2550 4700 2550
Wire Wire Line
	4700 3600 4400 3600
Wire Wire Line
	4750 2400 4650 2400
Wire Wire Line
	4750 2200 4600 2200
Wire Wire Line
	4750 1900 4550 1900
Wire Wire Line
	4650 3100 4400 3100
Wire Wire Line
	4650 2400 4650 3100
Wire Wire Line
	4600 3300 4400 3300
Wire Wire Line
	4600 2200 4600 3300
Wire Wire Line
	4400 3200 4550 3200
Wire Wire Line
	4550 1900 4550 3200
$Comp
L Switch:SW_Push SW2
U 1 1 5DBF4B96
P 2250 2200
F 0 "SW2" H 2450 2150 50  0000 R CNN
F 1 "SW_Push" H 2300 2150 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 2250 2400 50  0001 C CNN
F 3 "~" H 2250 2400 50  0001 C CNN
	1    2250 2200
	1    0    0    -1  
$EndComp
Text GLabel 2650 2600 2    50   Input ~ 0
VCC
Wire Wire Line
	2650 2600 2350 2600
Text GLabel 2650 2200 2    50   Input ~ 0
WAKE
$Comp
L power:GND #PWR0105
U 1 1 5DCFE827
P 1600 2200
F 0 "#PWR0105" H 1600 1950 50  0001 C CNN
F 1 "GND" H 1605 2027 50  0000 C CNN
F 2 "" H 1600 2200 50  0001 C CNN
F 3 "" H 1600 2200 50  0001 C CNN
	1    1600 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	1600 2200 2050 2200
Wire Wire Line
	2450 2200 2650 2200
Wire Wire Line
	4700 2550 4700 3600
$Comp
L MCU_Microchip_ATtiny:ATtiny13-20SSU U1
U 1 1 5E602F88
P 3800 3400
F 0 "U1" H 3271 3446 50  0000 R CNN
F 1 "ATtiny13-20SSU" H 3271 3355 50  0000 R CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3800 3400 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2535.pdf" H 3800 3400 50  0001 C CNN
	1    3800 3400
	1    0    0    -1  
$EndComp
Text GLabel 4750 2050 2    50   Input ~ 0
WAKE
Wire Wire Line
	4750 2050 4600 2050
Wire Wire Line
	4600 2050 4600 2200
Connection ~ 4600 2200
Wire Wire Line
	5200 2300 4650 2300
Wire Wire Line
	4650 2300 4650 2400
Connection ~ 4650 2400
Wire Wire Line
	5150 2650 5150 2500
Wire Wire Line
	5150 2500 5200 2500
Wire Wire Line
	5650 3200 4550 3200
Connection ~ 4550 3200
Wire Wire Line
	4600 3300 5650 3300
Connection ~ 4600 3300
Wire Wire Line
	5650 3400 4400 3400
Wire Wire Line
	4400 3500 5650 3500
$Comp
L Device:R_Pack04 RN1
U 1 1 5EEF9F50
P 5850 3300
F 0 "RN1" V 6175 3300 50  0000 C CNN
F 1 "10_X4" V 6084 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Concave_4x0603" V 6125 3300 50  0001 C CNN
F 3 "~" H 5850 3300 50  0001 C CNN
	1    5850 3300
	0    1    -1   0   
$EndComp
$Comp
L Connector:Conn_01x06_Male J1
U 1 1 60201BB3
P 2050 3350
F 0 "J1" H 2022 3324 50  0000 R CNN
F 1 "SPI" H 2022 3233 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2050 3350 50  0001 C CNN
F 3 "~" H 2050 3350 50  0001 C CNN
	1    2050 3350
	-1   0    0    -1  
$EndComp
Text Notes 6600 1950 0    50   ~ 0
This generic design uses standard SMT LEDs.\n\nAlternatively, side view or reverse mount LEDs may be used.\n\nFor side view LEDs to shine through the FR4, create a void in the silk, mask, and copper layers.\nReverse mount LEDs add a NPT hole, which the LED sets into.\n\nExample of side mount LEDs: American Opto Plus L170B-QOC-TR\nNOTE: when ordering side view LEDs look for Luminous Intensity above 120 if possible.\n\nExamples of reverse mount LEDs: Kingbright APTR3216SECK\nNOTE: a reverse mount LED has a square lens compared to a tranditional LED\nrectangular lens. It also has longer 'wings' to extend beyond the NPT hole for the lens.\n
Text Notes 7250 4400 0    50   ~ 0
The 4 pins can accomodate up to 12 LEDs\nthe 11th and 12 LED would be connected to the resistor array\nusing 7 and 8 and 8 and 7.
Text Notes 5500 3950 0    50   ~ 0
Charlieplexed LEDs\nresult in current limiting\nresistors on both leads.
$EndSCHEMATC
